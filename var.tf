variable "aws_region" {
    description = "EC2 Region for the VPC"
    default = "ap-south-1"
}
variable "availability_zone1" {
    description = "Avaialbility Zones"
    default = "ap-south-1a"
}

variable "availability_zone2" {
    description = "Avaialbility Zones"
    default = "ap-south-1b"
}
variable "main_vpc_cidr" {
    description = "CIDR of the VPC"
    default = "10.0.0.0/20"
}
variable "ami" {
    default = "ami-0123b531fc646552f"
}
variable "instance_type" {
    description = "Avaialbility Zones"
    default = "t2.micro"
}
variable "key" {
    description = "Avaialbility Zones"
    default = "nVirginia"
}
